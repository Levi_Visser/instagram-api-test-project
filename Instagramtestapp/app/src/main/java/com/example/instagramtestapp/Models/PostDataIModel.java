package com.example.instagramtestapp.Models;

public class PostDataIModel {
    private String thumbnailUrl, mediaID, standardResImage, caption;

    public PostDataIModel(String url, String mediaID, String standardResImage, String caption)
    {
        this.thumbnailUrl = url;
        this.mediaID = mediaID;
        this.standardResImage = standardResImage;
        this.caption = caption;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public String getMediaID() {
        return mediaID;
    }

    public String getStandardResImage() {
        return standardResImage;
    }

    public String getCaption() {
        return caption;
    }
}
