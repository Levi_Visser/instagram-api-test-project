package com.example.instagramtestapp.Interfaces;

public interface AuthenticationListener {
    void OnTokenReceived(String authToken);
}
