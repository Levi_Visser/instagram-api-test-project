package com.example.instagramtestapp.Models;

public class UserDataModel {
    public String ProfilePicture;
    public String FullName;

    public String getProfilePicture() {
        return ProfilePicture;
    }

    public String getFullName() {
        return FullName;
    }
}
