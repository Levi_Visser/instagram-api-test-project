package com.example.instagramtestapp;

import com.example.instagramtestapp.Adapters.GridViewImageAdapter;
import com.example.instagramtestapp.CustomDialogs.AuthenticationDialog;
import com.example.instagramtestapp.Interfaces.AuthenticationListener;
import com.example.instagramtestapp.Models.PostDataIModel;
import com.example.instagramtestapp.SaveData.AppPreferences;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/*
Main activity class, handles logging in, retrieving and displaying the users content
 */
public class MainActivity extends AppCompatActivity implements AuthenticationListener
{
    private String token = null;
    private AppPreferences appPreferences = null;
    private AuthenticationDialog authenticationDialog = null;
    private Button button = null;
    private View info = null;
    private GridView gridView;
    private GridViewImageAdapter adapter;
    private ArrayList<PostDataIModel> posts;
    private PostDataIModel clickedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        posts = new ArrayList<>();

        gridView = findViewById(R.id.grid_view);
        button = findViewById(R.id.btn_login);
        info = findViewById(R.id.info);

        appPreferences = new AppPreferences(this);
        adapter = new GridViewImageAdapter(this, R.layout.thumbnail_item, posts);
        gridView.setAdapter(adapter);

        //check if already have access token
        token = appPreferences.getString(AppPreferences.TOKEN);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Create Intent
                Intent intent = new Intent(getApplicationContext(), PostDetailActivity.class);
                clickedItem = (PostDataIModel) parent.getItemAtPosition(position);
                intent.putExtra("mediaID", clickedItem.getMediaID());
                intent.putExtra("standardResImageUrl", clickedItem.getStandardResImage());
                intent.putExtra("caption", clickedItem.getCaption());

                startActivity(intent);
            }
        });
    }

    public void login() {
        button.setText("LOGOUT");
        info.setVisibility(View.VISIBLE);
        ImageView pic = findViewById(R.id.pic);
        TextView name = findViewById(R.id.name);

        Picasso.with(this).load(appPreferences.getString(AppPreferences.PROFILE_PIC)).into(pic);
        name.setText(appPreferences.getString(AppPreferences.USER_NAME));
    }

    public void logout() {
        button.setText("INSTAGRAM LOGIN");
        token = null;
        info.setVisibility(View.GONE);
        appPreferences.clear();
        adapter.clear();
    }

    @Override
    public void OnTokenReceived(String authToken) {
        if (authToken == null)
            return;
        Log.i("AUTH TOKEN", "AUTH TOKEN RECEIVED");
        appPreferences.putString(AppPreferences.TOKEN, authToken);
        token = authToken;

        GetUserInfoByAccessToken();
        GetUSerMediaByAccessToken();
    }

    public void onClick(View view) {
        if(token!=null)
        {
            logout();
        }
        else {
            authenticationDialog = new AuthenticationDialog(this, this);
            authenticationDialog.setCancelable(true);
            authenticationDialog.show();
        }
    }

    private void GetUserInfoByAccessToken() {
        new RequestUserInfo().execute();
    }

    private void GetUSerMediaByAccessToken()
    {
        new RequestUserMedia().execute();
    }

    private PostDataIModel CreateNewPostMedia(JSONObject obj)
    {
        String mediaID = "";
        String thumbnailurl = "";
        String stanardResImgUrl = "";
        String caption = "";

        try{
            mediaID = obj.getString("id");
            thumbnailurl = obj.getJSONObject("images").getJSONObject("thumbnail").getString("url");
            stanardResImgUrl = obj.getJSONObject("images").getJSONObject("standard_resolution").getString("url");
            caption = obj.getJSONObject("caption").getString("text");
        } catch (JSONException e)
        {
            Log.e("POST OBJECT", "unexpected JSON exception", e);
        }

        return new PostDataIModel(thumbnailurl, mediaID, stanardResImgUrl, caption);
    }

    private class RequestUserInfo extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(Constants.USER_INFO_URL + token);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity httpEntity = response.getEntity();
                return EntityUtils.toString(httpEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("response", jsonObject.toString());
                    JSONObject jsonData = jsonObject.getJSONObject("data");
                    if (jsonData.has("id")) {
                        appPreferences.putString(AppPreferences.USER_ID, jsonData.getString("id"));
                        appPreferences.putString(AppPreferences.USER_NAME, jsonData.getString("username"));
                        appPreferences.putString(AppPreferences.PROFILE_PIC, jsonData.getString("profile_picture"));
                        login();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast toast = Toast.makeText(getApplicationContext(),"Login error!", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    private class RequestUserMedia extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(Constants.USER_MEDIA_URL + token);
            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity httpEntity = response.getEntity();
                return EntityUtils.toString(httpEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("user media response", jsonObject.toString());
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    for (int i =0; i < jsonArray.length(); i++)
                    {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        posts.add(CreateNewPostMedia(obj));
                        adapter.notifyDataSetChanged();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast toast = Toast.makeText(getApplicationContext(),"Login error!", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }
}
