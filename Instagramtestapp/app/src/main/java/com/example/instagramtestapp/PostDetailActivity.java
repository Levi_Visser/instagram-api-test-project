package com.example.instagramtestapp;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.instagramtestapp.Models.CommentListItem;
import com.example.instagramtestapp.SaveData.AppPreferences;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/*
Class used to display all the details about a post
 */
public class PostDetailActivity extends Activity {

    private ImageView postContentImage;
    private ImageView profileImage;
    private TextView userNameTextView;
    private TextView captionTextView;
    private String currentMediaID;
    private ArrayList<CommentListItem> comments;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_detail);

        comments = new ArrayList<CommentListItem>();
        postContentImage = findViewById(R.id.postImage);
        profileImage = findViewById(R.id.profilePicture);
        userNameTextView = findViewById(R.id.userName);
        captionTextView = findViewById(R.id.caption);

        LoadPostContent();
    }

    private void LoadPostContent()
    {
        //Get intent data
        Intent i = getIntent();
        currentMediaID = i.getExtras().getString("mediaID");

        SetupHeader();
        Picasso.with(this).load(i.getExtras().getString("standardResImageUrl")).into(postContentImage);
        captionTextView.setText(i.getExtras().getString("caption"));

        GetMediaComments();
    }

    private void SetupHeader()
    {
        Picasso.with(this).load(AppPreferences.Instance.getString(AppPreferences.PROFILE_PIC)).into(profileImage);
        userNameTextView.setText(AppPreferences.Instance.getString(AppPreferences.USER_NAME));
    }

    private void GetMediaComments()
    {
        new RequestComments().execute();
    }

    private CommentListItem CreateNewComment(JSONObject obj)
    {
        String comment = "";
        try
        {
            Log.e("user media response", obj.toString());
            comment = obj.getString("text");
        } catch (JSONException e)
        {
            Log.e("Comment Text", "unexpected JSON exception", e);
        }

        LinearLayout l = findViewById(R.id.CommentLayout);
        TextView textView = new TextView(this);
        textView.setText(comment);
        l.addView(textView);
        return new CommentListItem(comment);
    }

    private class RequestComments extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... params) {
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(
                    "https://api.instagram.com/v1/media/"
                    + currentMediaID
                    + "/comments?access_token="
                    + AppPreferences.Instance.getString(AppPreferences.TOKEN));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                HttpEntity httpEntity = response.getEntity();
                return EntityUtils.toString(httpEntity);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (response != null) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Log.e("Comment response", jsonObject.toString());
                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                    Log.i("JSON ARRAY LENGTH", "" +jsonArray.length());
                    for (int i =0; i < jsonArray.length(); i++)
                    {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        comments.add(CreateNewComment(obj));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast toast = Toast.makeText(getApplicationContext(),"Login error!", Toast.LENGTH_LONG);
                toast.show();
            }
        }


    }
}
