package com.example.instagramtestapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.example.instagramtestapp.Models.PostDataIModel;
import com.example.instagramtestapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GridViewImageAdapter extends ArrayAdapter<PostDataIModel> {

    public static GridViewImageAdapter Instance;
    private LayoutInflater inflater;

    //Constructor
    public GridViewImageAdapter(Context context, int resource, List<PostDataIModel> objects) {
        super(context, resource, objects);
        //Initialize the layout inflater
        Instance = this;
        inflater = LayoutInflater.from(getContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Inflate layout into the View for the Row
        convertView = inflater.inflate(R.layout.thumbnail_item, parent, false);
        PostDataIModel item = getItem(position);

        ImageView imageView = convertView.findViewById(R.id.thumbnail);

        Picasso.with(getContext()).load(item.getThumbnailUrl()).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        return imageView;
    }
}
