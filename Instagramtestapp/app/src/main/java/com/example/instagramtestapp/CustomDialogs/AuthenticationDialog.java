package com.example.instagramtestapp.CustomDialogs;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.instagramtestapp.Constants;
import com.example.instagramtestapp.Interfaces.AuthenticationListener;
import com.example.instagramtestapp.R;

/*
Used to authenticate the user on their first login
 */
public class AuthenticationDialog extends Dialog {

    private final String redirect_url;
    private final String request_url;
    private AuthenticationListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.auth_dialog);

        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(request_url);
        webView.setWebViewClient(webViewClient);
    }

    public AuthenticationDialog(@NonNull Context context, AuthenticationListener listener) {
        super(context);
        this.listener = listener;
        this.redirect_url = Constants.REDIRECT_URL;
        this.request_url = Constants.BASE_URL
                + "oauth/authorize/?client_id="
                + Constants.INSTAGRAM_CLIENT_ID
                + "&redirect_uri="
                + redirect_url
                + "&response_type=token&display=touch&scope=public_content";
    }

    WebViewClient webViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(redirect_url)) {
                AuthenticationDialog.this.dismiss();
                return true;
            }
            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.contains("access_token=")) {
                Uri uri = Uri.EMPTY.parse(url);
                String access_token = uri.getEncodedFragment();
                access_token = access_token.substring(access_token.lastIndexOf("=") + 1);
                listener.OnTokenReceived(access_token);
            }
        }
    };
}

