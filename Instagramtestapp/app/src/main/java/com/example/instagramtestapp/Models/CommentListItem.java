package com.example.instagramtestapp.Models;

public class CommentListItem {

    private String comment;

    public  CommentListItem(String comment){

        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }
}
